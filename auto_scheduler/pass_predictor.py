import math
from datetime import timedelta

import ephem


def overlap(satpass, scheduledpasses, wait_time_seconds):
    """Check if this pass overlaps with already scheduled passes"""
    # No overlap
    overlap = False

    # Add wait time
    tr = satpass['tr']
    ts = satpass['ts'] + timedelta(seconds=wait_time_seconds)

    # Loop over scheduled passes
    for scheduledpass in scheduledpasses:
        # Test pass falls within scheduled pass
        if tr >= scheduledpass['tr'] and ts < scheduledpass['ts'] + timedelta(
                seconds=wait_time_seconds):
            overlap = True
        # Scheduled pass falls within test pass
        elif scheduledpass['tr'] >= tr and scheduledpass['ts'] + timedelta(
                seconds=wait_time_seconds) < ts:
            overlap = True
        # Pass start falls within pass
        elif tr >= scheduledpass['tr'] and tr < scheduledpass['ts'] + timedelta(
                seconds=wait_time_seconds):
            overlap = True
        # Pass end falls within end
        elif ts >= scheduledpass['tr'] and ts < scheduledpass['ts'] + timedelta(
                seconds=wait_time_seconds):
            overlap = True
        if overlap:
            break

    return overlap


def create_observer(lat, lon, alt, min_riseset=0.0):
    '''
    Create an observer instance.
    '''
    observer = ephem.Observer()
    observer.lat = str(lat)
    observer.lon = str(lon)
    observer.elevation = alt
    observer.horizon = str(min_riseset)

    return observer


def find_passes(satellite, observer, tmin, tmax, minimum_altitude, min_pass_duration):
    passes = []

    # Set start time
    observer.date = ephem.date(tmin)

    # Load TLE
    try:
        sat_ephem = ephem.readtle(str(satellite.tle0), str(satellite.tle1), str(satellite.tle2))
    except (ValueError, AttributeError):
        return []

    # Loop over passes
    keep_digging = True
    while keep_digging:
        sat_ephem.compute(observer)
        try:
            tr, azr, tt, altt, ts, azs = observer.next_pass(sat_ephem)
        except ValueError:
            break  # there will be sats in our list that fall below horizon, skip
        except TypeError:
            break  # if there happens to be a non-EarthSatellite object in the list
        except Exception:
            break

        if tr is None:
            break

        # using the angles module convert the sexagesimal degree into
        # something more easily read by a human
        try:
            elevation = format(math.degrees(altt), '.0f')
            azimuth_r = format(math.degrees(azr), '.0f')
            azimuth_s = format(math.degrees(azs), '.0f')
        except TypeError:
            break

        pass_duration = ts.datetime() - tr.datetime()

        # show only if >= configured horizon and till tmax,
        # and not directly overhead (tr < ts see issue 199)

        if tr < ephem.date(tmax):
            if (float(elevation) >= minimum_altitude and tr < ts
                    and pass_duration > timedelta(minutes=min_pass_duration)):

                # get pass information
                satpass = {
                    'tr': tr.datetime(),  # Rise time
                    'azr': azimuth_r,  # Rise Azimuth
                    'tt': tt.datetime(),  # Max altitude time
                    'altt': elevation,  # Max altitude
                    'ts': ts.datetime(),  # Set time
                    'azs': azimuth_s,  # Set azimuth
                    'transmitter': {
                        'uuid': satellite.transmitter,
                        'success_rate': satellite.success_rate,
                        'good_count': satellite.good_count,
                        'data_count': satellite.data_count,
                        'mode': satellite.mode,
                    },
                    'scheduled': False
                }
                passes.append(satpass)
            observer.date = ephem.Date(ts).datetime() + timedelta(minutes=1)
        else:
            keep_digging = False

    return passes


def constrain_pass_to_az_window(satellite, observer, satpass, start_azimuth, stop_azimuth,
                                min_pass_duration):
    """
    Modifies the observation start/stop time to satisfy azimuth viewing window constraints.
    For example, if there is an obstruction covering the start of the pass then the pass start time
    will be adjusted to begin only when the satellite has cleared the obstruction. This can result
    in shorter observations and more time-efficient scheduling.
    :param satellite: Satellite object the satpass is for
    :param observer: Observer
    :param satpass: Satpass to be adjusted to fit within the viewing window. Modified in-place.
    :param start_azimuth: Start of the viewing window. Viewing window is the area covered by a
    clockwise sweep from the start angle to the end angle.
    :param stop_azimuth: End of the viewing window.
    :param min_pass_duration: Minimum pass duration in minutes. Passes shorter than this are
    discarded.
    :return: The modified satpass object that satisfies the viewing window constraint, or None if
    it is impossible to satisfy the viewing window and minimum pass duration constraints
    """

    # How much the start/stop time should be incremented by each step while finding the
    # parameters for the constrained pass. Larger values will solve quicker but will result in a
    # worse solution.
    sweep_step_size = timedelta(seconds=1)

    # Load TLE
    try:
        sat_ephem = ephem.readtle(str(satellite.tle0), str(satellite.tle1), str(satellite.tle2))
    except (ValueError, AttributeError):
        return None

    # Sweep the start of pass time forwards until the azimuth constraint is met, or another
    # constraint fails
    azr_within_window = False
    while not azr_within_window:
        # Set pass start time
        observer.date = ephem.date(satpass['tr'])
        sat_ephem.compute(observer)

        # Convert to degrees
        satpass['azr'] = format(math.degrees(sat_ephem.az), '.0f')
        pass_duration = satpass['ts'] - satpass['tr']

        azr_within_window = check_az_in_window(float(satpass['azr']), start_azimuth, stop_azimuth)

        if not azr_within_window:
            # Change the pass start time for the next iteration
            satpass['tr'] += sweep_step_size

        if pass_duration < timedelta(minutes=min_pass_duration):
            return None

    # Sweep the end of pass time backwards until the azimuth constraint is met, or another
    # constraint fails
    azs_within_window = False
    while not azs_within_window:
        # Set pass stop time
        observer.date = ephem.date(satpass['ts'])
        sat_ephem.compute(observer)

        # Convert to degrees
        satpass['azs'] = format(math.degrees(sat_ephem.az), '.0f')

        azs_within_window = check_az_in_window(float(satpass['azs']), start_azimuth, stop_azimuth)
        pass_duration = satpass['ts'] - satpass['tr']

        if not azs_within_window:
            # Change the pass stop time for the next iteration
            satpass['ts'] -= sweep_step_size

        if pass_duration < timedelta(minutes=min_pass_duration):
            return None

    return satpass


def check_az_in_window(azimuth, start_azimuth, stop_azimuth):
    """
    Determines whether a given azimuth angle is between two specified start/stop azimuth angles.
    In effect, checks that an azimuth is within an acceptable viewing window.
    :param azimuth: Azimuth to be tested
    :param start_azimuth: Start of the viewing window. Viewing window is the area covered by a
    clockwise sweep from the start angle to the end angle.
    :param stop_azimuth: End of the viewing window.
    :return: True if the specified azimuth is contained inside the viewing window.
    """

    # Determine if the specified viewing window crosses zero degrees
    # If so, we perform the window check with a complementary window and invert the result
    if start_azimuth > stop_azimuth:
        complementary_window = True

        # Swap start and stop which inverts the window and avoids the zero crossing
        temp = start_azimuth
        start_azimuth = stop_azimuth
        stop_azimuth = temp
    else:
        complementary_window = False

    # Check if the specified azimuth is inside the window
    if start_azimuth <= azimuth <= stop_azimuth:
        # Azimuth is within the window (normal or complementary)
        if complementary_window:
            return False
        else:
            return True

    # Azimuth is outside the window (normal or complementary)
    if complementary_window:
        return True
    else:
        return False
